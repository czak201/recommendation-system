
### Spis treści
1.  Entity Framework
    1.1  Klasy dla bazy danych
    1.2 Tworzenie bazy danych i jej aktualizacja
2. Repozytoria klas i ich interfejsy
	2.1 LinkRepository
	2.2 MovieRepository
	2.3 RatingRepository
	2.4 TagRepository
	2.5 UserRepository
3. Rest Client
4. Klasa do Generowania JSON dla pythona.
5. Nadawanie ID nowemu użytkownikowi

#### 1. Entity framework
##### 1.1. Klasy dla bazy danych
Folder ``EFModel``zawiera klasy potrzebne do utworzenia i obsługi bazy danych.

Klasy ``Link``, ``Movie``, ``Rating``, ``Tag`` są utworzone na wzór gotowych plików _.csv_ aby nie było problemów z importowaniem danych.
Klasa ``User`` jest potrzebna do przypisania każdemu użytkownikowi ID w typie integer, ponieważ jest ono wymagane przy obliczaniu predykcji a domyślne ID wygenerowane przez microsoftowy system rejestracji wygląda następująco: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
Klasa ``EFContext`` jest potrzebna do wygenerowania odpowiednich tablic w bazie danych.

##### 1.2. Tworzenie bazy danych i jej aktualizacja
Baza danych jest obsługiwana na lokalnym serwerze SQL EXPRESS.
Kopia bazy danych jest dołączona do projektu.
Adres połączenia z bazą danych znajduje się w pliku ``appsettings.json`` w polu *DefaultConnection*.
Tworzona jest metodą "Code First"
Po utworzeniu klas ``Link``, ``Movie``, ``Rating``, ``Tag`` możemy przystąpić do utworzenia migracji, czyli automatycznie wygenerowanego kodu który utworzy/zautkalizuje bazę danych.

Adres połączenia z bazą danych znajduje się w pliku ``appsettings.json`` w katalogu głównym projektu.

Kod do utworzenia migracji (wpisywany w Package Manager Console):
```
dotnet ef migrations add nazwa_migracji --Context EFContext
```
Kod do aktualizacji bazy danych:
```
dotnet ef database update --Context EFContext
```
Znane problemy:
 * Przy wystąpieniu błędu podczas aktualizacji typu "Tabela jest już utworzona" należy usunąć wszystkie stare pliki migracji z folderu ``Migrations`` i ponowić aktualizację
 * Podczas tworzenia migracji może pojawić się komunikat o nie znalezieniu plików projektu. Wówczas w konsili trzeba przejść do odpowiedniego folderu komendą: *cd .\WebApp*

#### 2. Repozytoria klas i ich interfejsy
Repozytoria i ich interfejsy znajdują się w folderze ``Repository`` i ``Interfaces``. Repozytoria zawierają funkcje potrzebne do obsługi tabel w bazie danych.
##### 2.1. LinkRepository
LinkRepository jest wykorzystywany do pobierania specjalnych identyfiakorów filmów dzięki którym jest możliwość pobrania plakatów filmów z zewnętrznych serwisów.
*Funkcje:*
``Get`` przyjmuje parametr ``id`` który jest unikalnym identyfikatorem filmu. Zwraca obiekt typu ``Link`` zawierający unikalne identyfikatory które są wykorzystywane w serwisach *imdb.pl* i *tmdb.pl*
``Add`` dodaje obiekt do bazy danych. (Nie jest używana)
##### 2.2 MovieRepository
MovieRepository jest wykorzystywany do pobierania wszelkich informacji o filmach oraz generowania losowych filmów na potrzeby tworzenia ocen. Wszystkie informacje pobierane są z bazy danych MS SQL.

*Funkcje:*
``Add`` dodaje do bazy danych nowy film. (Nie jest używana)

``Get`` po podaniu parametru id filmu zwraca podstawowe dane na temat filmu: *id, tytuł, gatunki*

``GetMovie`` przyjmuje parametr id filmu i zwraca szczegółowe dane na temat filmu: *id,tytuł, rok, ilość ocen, średnia ocen, gatunki, link url dla imdb.pl oraz tmdb.pl*

``GetNPopularMoviesByGenre`` przyjmuje parametry ``int n``-ilość filmów, ``genre``-nazwa gatunku. Zwraca n-listę filmów które są popularne w wybranym gatunku

``GetNPopularMovies`` przyjmuje parametr `` int NumberOfMovies``-ilość filmów. Zwraca n-listę popularnych filmów w całej bazie danych.

``GetNBestMovies`` przyjmuje parametr ``int NumberOfMovies``-ilość filmów. Zwraca n-listę najlepiej ocenianych i oglądanych filmów w całej bazie danych.

``GetNBestMovies``  przyjmuje parametr ``int NumberOfMovies``-ilość filmów. Zwraca n-listę najlepiej ocenianych filmów w całej bazie danych.

``FindAllMoviesByTitle`` przyjmuje parametr ``string wordInTitle`` który jest dowolnym ciągiem znaków. Zwraca listę wszystkich filmów które zawierają wymagany ciąg znaków w tytule.

``FindNMoviesByTitle`` przyjmuje parametr ``string wordInTitle`` który jest dowolnym ciągiem znaków oraz parametr ``int n`` -ilość filmów. Zwraca listę wszystkich filmów które zawierają wymagany ciąg znaków w tytule.

``GetMoviesByID`` przyjmuje parametr ``int[] movieId``- tablica id filmów. Zwraca listę filmów ze szczegółowymi danymi.

``FindNMoviesByGenre`` przyjmuje parametry ``string genre`` - nazwa gatunku i ``int iloscFilmow`` - ilość wyświetlanych filmów. Zwraca ograniczoną listę filmów które posiadają wybrany gatunek.

``GetNRandomMoviesByGenre`` przyjmuje parametry ``int n``- ilość filmów i ``string genre``- gatunek wybrany przez usera. Zwraca listę filmów ze wskazanego gatunku w losowej kolejności.

``GetNRandomMovies``przyjmuje parametry ``int n``- ilość filmów. Zwraca listę losowych filmów z całej bazy danych.

##### 2.3 RatingRepository
*Funkcje:*
``Add`` dodaje do bazy danych nową ocenę.
``AddList``dodaje do bazy danych nową listę ocen pobieraną ze strony.
``Get``zwraca informację na temat oceny (id usera, id filmu, ocenę, timestamp)

##### 2.4 TagRepository
Posiada dwie funkcje: ``Add`` i ``Get``. Funkcje nie są nigdzie używane. Został stworzone tylko dla obsługi datasetu w dalekiej przyszłości.

##### 2.5 UserRepository
Posiada funkcje dodawania użytkownika oraz pobierania jego id według parametru (int) id lub (microsoft) id na potrzeby generowania predykcji w pythonie.

#### 3. Rest Client
Klasa posiada funkcje do komunikacji z restem oraz odbierania od niego predykcji.
*Funkcje:*
``ReturnJsonForPython``przyjmuje parametry ``int id``- id zalogowanego użytkownika i ``RatingList ratings``- listę ocenionych filmów przez użytkownika. Zwraca JSON którego można wysłać do pythona.
Wykorzystuje klasę /Classes/UserRatingList.cs

``AddUser`` po podaniu JSON który jest tworzony w /Controllers/AccountController w funkcji register, wysyła do pythona id nowego użytkownika i ocenę pierwszego filmu na 0.0 aby został on utworzony w modelu w predykcjach.

``UpdateUser``przyjmuje parametr JSON w którym jest id użytkownika oraz jego oceny które są wprowadzane do modelu w pythonie.

``GetAllPred``po podaniu id usera zwraca wszystkie proponowane filmy (jest zablokowane tymczasowo na 10 aby nie zużyć wszystkich requestów w imdb.pl)

``GetTopN``- po podaniu id usera i n-liczby filmów zwraca n-najlepszych filmów dla danego usera.

``Remodel``- zdalna aktualizacja modelu w pythonie.

``GetNPredictEmptyUser``- zwraca proponowane filmy dla usera który nigdy nic nie ocenił.

#### 4. Klasa do Generowania JSON dla pythona.
Klasa do generowania JSON który jest potrzebny do stworzenia listy z ocenami znajduje się w /Classes/UserRatingList.cs
Po podaniu ocen przez użytkownika w /Controllers/RatingController generowany jest obiekt który zawiera id użytkownika, liczbę ocenionych filmów oraz filmy. Tworzenie JSON z tego obiektu jest w klasie /Classes/RestClient.cs w funkcji ReturnJsonForPython. 
Przykładowy JSON:
````
{
	"user": "672",
	"liczbaFilmow" : "3",
	"oceny": [{
			"movieId": "1",
			"rating": "0.2"
		},
		{
			"movieId": "2",
			"rating": "0.4"
		},
		{
			"movieId": "4",
			"rating": "0.5"
		}
	]
}
````
#### 5. Nadawanie ID nowemu użytkownikowi
Generowanie predykcji wymaga podawania id typu integer. Gotowa klasa dla rejestracji generuje id w stylu: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.
W ``/Controllers/AccoutControllers`` w funkcji ``Register``, po poprawnej rejestracji, w bazie danych w tabeli ``Users``jest generowane id typu integer i przypisywane do niego microsoftowe id. Na potrzeby testowe możemy zmienić generowanie id od pewnej liczby wpisując querry:
```
DBCC CHECKIDENT ('dbo.Users', RESEED, nasze_id)
	GO
```
Po nadaniu id, do pythona jest wysyłana wiadomość o utworzeniu usera i ocenieniu filmu o id "1" na ocenę "0.0".