# Spis treści


[TOC]

# 0. Uruchomienie serwera Rest

### 0.1 Załaduj pliki projektu do Pycharm IDE.
### 0.2 Zainstaluj dodatkowe oprogramowanie, tak aby w *Project Interpreter*'ze były następujące biblioteki: Flask, Pandas, Numpy, Scipy, Json
### 0.3 W pliku REST.py zmień konfigurację na zgodną z Twoim systemem (ip, port)
### 0.4 W Pycharm IDE uruchom plik REST.py


# 1. Requests to Flask Rest Server (REST.py):

### 1.0 Konfiguracja

#### 1.0.1 emptyUser - user, ktory na nic nie glosowal

#### 1.0.2 url: http://*ip_servera*:*port*

### 1.1 url/about


return: "about page"


output: string


### 1.2 url/userN/<username>/<ile>

input:


 *username*: int, userId
 
 
*ile*: int, ile sztuk filmow zwrocic 


return "ile" predictions for user "username"

zwraca obiekt json z lista numerow movieId;
wykorzystuje metodę giveFastPred


output json

```
[
    6887,
    7839,
    4571,
    2129,
    8724,
    ...
]
```
 
 
### 1.3 default(o)


metoda do obsługi buga w obsłudze json przez python;
zamienia numpy.int64 na zwykły int


### 1.4 url/updateUser


return: json ze stringiem :"user updated successfully",


aktualizuje dane usera w bazie csv i przelicza model na nowo

input:


rating - float z zakresu (0,1]


json:

```
{
	"user": "672",
	"liczbaFilmow" : "2",
	"oceny": [{
			"movieId": "1",
        	"rating": "0.1"
		},
        {
			"movieId": "2",
        	"rating": "0.1" 
		}
       ]
}
```

output json[ "user updated successfully" ]


### 1.5 url/newUser


return 0, 


tworzy nowego usera w bazie csv i przelicza model na nowo, ładuje predykcje do pamieci


input:


rating - float z zakresu (0,1]


 json:

```
{
	"user": "672",
	"liczbaFilmow" : "3", 
	"oceny": [{
   		"movieId": "1",
  		"rating": "0.2"
  		},
        {
        	"movieId": "2",
        	"rating": "0.4"
      	},
      	{
      		"movieId": "4",
      		"rating": "0.5"
      	}
      ]
}
```

output 0

### 1.6 url/remodel

przelicza model na nowo i 
laduje do pamieci jsona z odpowiedzią na zadanie predykcji dla emptyUser,
 
 
output 0


### 1.7 url/emptyUser


return predictions dla emptyUser,


output json


```
[
    6887,
    7839,
    4571,
    2129,
    8724,
    ...
 ]
```

### 1.8 url/newUserId
zwraca int: numer_id ostatniego usera w pliku csv

### 1.9 onStarted()


metoda wywoływana przed obsłuzeniem pierwszego żadania,


inicjalizuje model, predykcje dla emptyUser
 
 
# 2. Dodawanie i update userow w csv (UserInfoUpd.py)
 
 
### 2.1 load_csv(path)


laduje plik csv do pamieci


input:


*path* - string: ścieżka pliku


output: pandas dataframe



### 2.2 updateUserTF(userId, ratings, movieIds):


aktualizuje oceny uzytkownika o podanym id w pliku csv


input:


*userId* - int: id usera


*ratings* - float []: tablica z ocenami filmow


*movieIds* - int []: tablica z ocenionymi movie id


output: 0


### 2.3 createUserTF(userId, ratings, movieIds):

dodaje nowego uzytkownika do pliku csv
inicjalizuje wszystkie oceny uzytkownika na zero
, chyba że są one podane w json'ie
i zapisuje je w pliku csv;


input:


*userId* - int: id usera


*ratings* - float []: tablica z ocenami filmow


*movieIds* - int []: tablica z ocenionymi movie id


output: indeks nowego usera

### 2.4 dajLiczbeUserow()
 
 zwraca int: numer_id ostatniego usera w pliku csv
 
 
# 3. BackEnd, faktoryzacja macierzy, predykcje (PredictionEngine.py)
 
 
### 3.1 loadPreds():

faktoryzuje maciez ocen userow i tworzy predykcje; przechowuje je w pamieci

Faktoryzacja macierzy odbywa się na zasadach Collaborative filtering i Low-Rank Matrix Factorization.
Konkretnie korzysta się tu z "singular value decomposition SVD", która dekomponuje macierz ocen na
3 macierze. 


### 3.2 giveFastPred(userId, topN):


input: 


*userId* - int: id usera


*topN* - int : liczba predykcji do zwrocenia 


output: predykcje sztuk topN dla usera *userId*
 