import numpy as np
import pandas as pd
import os
from scipy.sparse.linalg import svds
import csv

train_data_out_path = os.getcwd()+"/CSV/out.csv"

preds = []

def loadPreds():
    pwd = os.getcwd()
    print("super fast max factor")
    os.chdir(os.path.dirname(train_data_out_path))
    train_data = pd.read_csv(os.path.basename(train_data_out_path), quotechar="\"", sep=',\s+', doublequote=True,
                             quoting=csv.QUOTE_ALL, delimiter=",", error_bad_lines=False)
    os.chdir(pwd)


    X_train = train_data
    X_train = X_train.drop(X_train.columns[[0]], 1)

    R_df = X_train
    R = R_df.values
    user_ratings_mean = np.mean(R, axis=1)
    R_demeaned = R - user_ratings_mean.reshape(-1, 1)

    U, sigma, Vt = svds(R_demeaned, k=50)

    sigma = np.diag(sigma)

    all_user_predicted_ratings = np.dot(np.dot(U, sigma), Vt) + user_ratings_mean.reshape(-1, 1)
    preds_df = pd.DataFrame(all_user_predicted_ratings, columns=R_df.columns)


    listaNazwKol = list(X_train)
    temp = []
    for userId in range(X_train.shape[0]):

        sorted_user_predictions = preds_df.iloc[userId].sort_values(ascending=False)
        sample_user = X_train.iloc[userId, :]
        voted = np.nonzero(sample_user)[0]

        voted2 = []

        #zwraca liste moviesId
        for i, ind in enumerate(voted):
            voted2.append(listaNazwKol[ind])

        listaPredMovId = sorted_user_predictions.keys().tolist()

        # wycina filmy na ktore user juz glosowal
        voted3 = [x for x in listaPredMovId if x not in voted2]

        temp.append(voted3)
        # preds.append(voted3)

        if userId  % 100 == 0:
            print(userId )

    global preds
    preds = temp
    return 0

def giveFastPred(userId, topN):

    prediscts = preds[userId]
    forem = []

    if topN > len(prediscts):
        topN = len(prediscts)

    if topN > 0:
        forem = prediscts[0:topN]
    else:
        forem = prediscts[0:len(prediscts)]
    return forem
