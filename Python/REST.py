from flask import Flask, request
from flask_restful import Resource, Api

from Python.UserInfoUpd import createUserTF
from Python.UserInfoUpd import updateUserTF
from Python.UserInfoUpd import dajLiczbeUserow

from Python.PredictionsEngine import giveFastPred
from Python.PredictionsEngine import loadPreds

import json
import numpy
import time

host = '192.168.0.192'
port = '5002'

app = Flask(__name__)
api = Api(app)

emptyUserPreds = None


@app.route('/userN/<username>/<ile>', methods=['POST', 'GET'])
def show_user_profileN(username, ile):
    start = time.time()

    print(request.environ['REMOTE_ADDR'])
    print(int(username))
    print('in show user N/ ile profie method')
    pred = giveFastPred(int(username), int(ile))

    print(pred)

    end = time.time()
    print("Czas przygotowania listy preds:")
    print(end - start)

    start = time.time()
    response = app.response_class(
        response=json.dumps(pred, default=default),
        status=200,
        mimetype='application/json'
    )
    end = time.time()
    print("Czas przygotowania json'a :")
    print(end - start)
    return response


def default(o):
    if isinstance(o, numpy.int64): return int(o)
    raise TypeError


@app.route('/updateUser', methods=['POST', 'GET'])
def updateUserJson():

    print(request.environ['REMOTE_ADDR'])
    print('in show updateUser profie method')

    req_data = request.get_json()

    print(req_data)

    user = req_data['user']

    print(user)

    liczbaFilmow = req_data['liczbaFilmow']
    liczbaFilmow = int(liczbaFilmow)

    print(liczbaFilmow)

    rating = []
    movieIds = []

    for i in range(liczbaFilmow):
        liczba = int(i)

        print(req_data['oceny'][liczba]['rating'])
        rating.append((req_data['oceny'][liczba]['rating']))
        print(req_data['oceny'][liczba]['movieId'])
        movieIds.append(req_data['oceny'][liczba]['movieId'])

    updateUserTF(user, rating, movieIds)

    loadPreds()

    response = app.response_class(
        response=json.dumps("user updated successfully", default=default),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/newUser', methods=['POST', 'GET'])
def newUserJson():
    print(request.environ['REMOTE_ADDR'])
    print('in show new User profie method')
    req_data = request.get_json()
    user = req_data['user']
    liczbaFilmow = req_data['liczbaFilmow']
    liczbaFilmow = int(liczbaFilmow)
    rating = []
    movieIds = []

    for i in range(liczbaFilmow):

        liczba = int(i)

        print(req_data['oceny'][liczba]['rating'])
        rating.append((req_data['oceny'][liczba]['rating']))
        print(req_data['oceny'][liczba]['movieId'])
        movieIds.append(req_data['oceny'][liczba]['movieId'])
    indeksNowegoUsera = createUserTF(user, rating, movieIds)

    loadPreds()
    #return 0
    response = app.response_class(
        response=json.dumps("user created successfully", default=default),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/newUserId', methods=['POST', 'GET'])
def returnSafeId():
    print(request.environ['REMOTE_ADDR'])
    print('in gimme last user id')
    return dajLiczbeUserow()

@app.route('/remodel', methods=['POST', 'GET'])
def remodel():
    print(request.environ['REMOTE_ADDR'])
    print('in remodel method')

    pred = loadPreds()
    global emptyUserPreds
    emptyUserPreds = app.response_class(
        response=json.dumps(pred, default=default),
        status=200,
        mimetype='application/json'
    )


    global flagaEmpty
    flagaEmpty = True
    print("finished ok, Czas wykonania learning wynosi: " + str(0))
    # s = "finished ok, Czas wykonania learning wynosi: " + str(t)
    # response = app.response_class(
    #     response=json.dumps(s, default=default),
    #     status=200,
    #     mimetype='application/json'
    # )
    return emptyUserPreds

@app.route('/emptyUser', methods=['POST', 'GET'])
def empty():
    print(request.environ['REMOTE_ADDR'])
    print('in show empty profie method')

    return emptyUserPreds

@app.route('/about')
def about():
    print(request.environ['REMOTE_ADDR'])
    return 'xDD, U\'ve just got zetted'


@app.before_first_request
def onStarted():
    print(request.environ['REMOTE_ADDR'])
    print("in on start")

    loadPreds()
    pred = giveFastPred(671, -1)
    print(pred)
    global emptyUserPreds
    emptyUserPreds = app.response_class(
        response=json.dumps(pred, default=default),
        status=200,
        mimetype='application/json'
    )
    return True


if __name__ == '__main__':
    app.run(host=host, port=port)