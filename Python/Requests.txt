Requests to Flask Rest Server:

http://192.168.0.192:5002/about
return about page
output string

http://192.168.0.192:5002/user10/671
return top10 predictions for user 671
output json
[
    6887,
    7839,
    4571,
    2129,
    8724,
    ...
]

http://192.168.0.192:5002/userAll/671
return all predictions for user 671
output json
[
    6887,
    7839,
    4571,
    2129,
    8724,
    ...
]

http://192.168.0.192:5002/remodel
return void, just create new model from data, visit every hour or so
output string

http://192.168.0.192:5002/newUser
return prediction for new user, using old model
input json:
{
	"user": "672",
	"liczbaFilmow" : "3",
	"oceny": [{
			"movieId": "1",
			"rating": "0.2"
		},
		{
			"movieId": "2",
			"rating": "0.4"
		},
		{
			"movieId": "4",
			"rating": "0.5"
		}
	]
}
output json
[
    6887,
    7839,
    4571,
    2129,
    8724,
    ...
]

http://192.168.0.192:5002/updateUser
return prediction for already existed user, using old model,
but also updating data on which new model will be based
input json:
{
	"user": "672",
	"liczbaFilmow" : "2",
	"oceny": [{
			"movieId": "1",
			"rating": "0.1"
		},
		{
			"movieId": "2",
			"rating": "0.1"
		}
	]
}
output json
[
    6887,
    7839,
    4571,
    2129,
    8724,
    ...
]

http://192.168.0.192:5002/emptyUser
return predictions for users which never voted,
output json
[
    6887,
    7839,
    4571,
    2129,
    8724,
    ...
]