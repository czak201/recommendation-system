import pandas as pd
import csv
import os


train_data_out_path = os.getcwd()+"/CSV/out.csv"

def load_csv(path):
    pwd = os.getcwd()
    # print(path)
    os.chdir(os.path.dirname(path))
    train_data = pd.read_csv(os.path.basename(path), quotechar="\"",sep=',\s+', doublequote=True, quoting=csv.QUOTE_ALL, delimiter=",", error_bad_lines=False)
    os.chdir(pwd)
    return train_data


def dajLiczbeUserow():
    path = train_data_out_path
    X_train = load_csv(path)
    return X_train.shape[0]


#dodaje nowego usera do csv out
def createUserTF(user, rating, movieIds):
    path = train_data_out_path
    X_train = load_csv(path)

    X_train = X_train.drop(X_train.columns[[0]], 1)


    buckets = [0] * X_train.shape[1]
    indeks = X_train.shape[0]
    X_train.loc[indeks] = buckets
    for i, row in enumerate(movieIds):
        X_train.at[indeks, str(row)] = float(rating[i])
    print(X_train)

    X_train.to_csv(train_data_out_path, sep=',', encoding='utf-8')
    return indeks

# updatatuje starego usera i jego oceny filmow
def updateUserTF(user, rating, movieIds):
    path = train_data_out_path
    X_train = load_csv(path)

    X_train = X_train.drop(X_train.columns[[0]], 1)


    print(user)
    print(rating)
    print(movieIds)

    for i, row in enumerate(movieIds):
        X_train.at[int(user), str(row)] = float(rating[i])
    print(X_train)

    X_train.to_csv(
        train_data_out_path, sep=',', encoding='utf-8')
    return 0


















