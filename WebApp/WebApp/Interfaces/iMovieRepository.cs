﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Interfaces
{
    public interface iMovieRepository
    {
        EFModel.Movie Get(int id);
        void Add(object obj);
        Classes.Movie GetMovie(int id);
        List<Classes.Movie> GetNPopularMovies(int n);
        List<Classes.Movie> GetNBestMovies(int NumberOfMovies);
        List<Classes.Movie> GetNPopularMoviesByGenre(int n, string genre);
        List<Classes.Movie> GetMoviesByID(int[] movieIds);
        List<Classes.Movie> FindAllMoviesByTitle(string wordInTitle);
        List<Classes.Movie> FindNMoviesByTitle(string wordInTitle, int n);
        List<Classes.Movie> FindNMoviesByGenre(string word, int iloscFilmow);

        List<Classes.Movie> GetNRandomMoviesByGenre(int NumberOfMovies, string MovieGenre);
        List<Classes.Movie> GetNRandomMovies(int n);
    }
}
