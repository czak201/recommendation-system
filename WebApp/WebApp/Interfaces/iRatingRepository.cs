﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;
using WebApp.EFModel;

namespace WebApp.Interfaces
{
    public interface iRatingRepository
    {
        Rating Get(int id);
        void Add(object obj);
        void AddList(RatingList model);
    }
}
