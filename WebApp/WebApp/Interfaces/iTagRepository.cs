﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.EFModel;

namespace WebApp.Interfaces
{
    interface iTagRepository
    {
        Tag Get(int id);
        void Add(object obj);

    }
}
