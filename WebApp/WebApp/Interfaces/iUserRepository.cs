﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.EFModel;

namespace WebApp.Interfaces
{
    public interface iUserRepository
    {
        void Add(object Add);
        User GetUserById(int id);
        User GetUserByIdentity(string identity);
    }
}
