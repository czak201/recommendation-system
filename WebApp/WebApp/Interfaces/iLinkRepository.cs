﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.EFModel;

namespace WebApp.Interfaces
{
    interface iLinkRepository
    {
        Link Get(int id);
        void Add(object obj);
    }
}
