﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Interfaces
{
    public interface iRestClient
    {
        string ReturnJsonForPython(int id, RatingList ratings);
        void AddUser(string jsonMsg);
        void UpdateUser(string jsonMsg);
        int[] GetAllPred(int id);
        int[] GetTopN(int id, int n);
        void Remodel();
        int[] GetNPredictEmptyUser(int n);
    }
}
