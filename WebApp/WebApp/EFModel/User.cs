﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.EFModel
{
    public class User
    {
        [Key]
        public int userId { get; set; }

        public string userIdentity { get; set; }
    }
}
