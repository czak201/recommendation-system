﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.EFModel
{
    public class Movie
    {
        [Key]
        public int movieId { get; set; }

        public string title { get; set; }
        public string genres { get; set; }
    }
}
