﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.EFModel
{
    public class Tag
    {
        [Key]
        public int userId { get; set; }

        public int movieId { get; set; }
        public string tag { get; set; }
        public int timestamp { get; set; }
    }
}
