﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.EFModel
{
    public class Rating
    {
        [Display(Name = "User ID")]
        public int userId { get; set; }
        [Display(Name = "Movie ID")]
        public int movieId { get; set; }
        [Display(Name = "Rating")]
        public float rating { get; set; }
        [Key]
        [Display(Name = "Timestamp")]
        public int timestamp { get; set; }
    }
}
