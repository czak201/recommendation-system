﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.EFModel
{
    public class Link
    {
        [Key]
        public int movieId { get; set; }

        public int imdbId { get; set; }
        public int tmdbId { get; set; }
    }
}
