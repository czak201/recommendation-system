﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.EFModel;
using WebApp.Interfaces;
using WebApp.Classes;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace WebApp.Repository
{
    public class MovieRepository : iMovieRepository
    {
        private readonly EFContext _context;
        public MovieRepository(EFContext context)
        {
            _context = context;
        }

        public void Add(object obj)
        {
            _context.Add(obj);
            _context.SaveChanges();
        }

        public EFModel.Movie Get(int MovieId)
        {
            return _context.Movies.FirstOrDefault(p => Convert.ToInt32(p.movieId) == MovieId);
        }
        public Classes.Movie GetMovie(int MovieId)
        {
            try
            {
                float movieAvg;
                int movieCount;

                try
                {
                    movieAvg = _context.Ratings.Where(c => c.movieId == MovieId).Average(c => c.rating);
                    movieCount = _context.Ratings.Where(c => c.movieId == MovieId).Count();
                }
                catch (Exception e)
                {
                    movieAvg = 0;
                    movieCount = 0;
                }
                var imdbId = _context.Links.FirstOrDefault(c => c.movieId == MovieId);
                EFModel.Movie movie = _context.Movies.FirstOrDefault(p => p.movieId == MovieId);
                return new Classes.Movie(movie.movieId, movie.title, movie.genres, movieAvg, movieCount, imdbId.imdbId);
            }
            catch (Exception e)
            {
                return new Classes.Movie();
            }
        }
        public List<Classes.Movie> GetNPopularMoviesByGenre(int n, string genre)
        {
            List<Classes.Movie> listMovies = new List<Classes.Movie>();
            var topn = (from rl in _context.Ratings
                        group rl by rl.movieId into rlg
                        join ml in _context.Movies on rlg.FirstOrDefault().movieId equals ml.movieId
                        select new
                        {
                            MovieId = ml.movieId,
                            Genres = ml.genres,
                            Count = rlg.Count(),
                            Average = rlg.Average(x => Convert.ToDecimal(x.rating, CultureInfo.InvariantCulture)),
                        }).Where(x => x.Genres.Contains(genre)).Where(x => x.Count >= 10).OrderByDescending(x => x.Count);
            var topUnique = (from x in topn select x).Distinct();
            foreach (var Movie in topUnique.Take(n))
            {
                listMovies.Add(GetMovie(Movie.MovieId));
            }
            return listMovies;
        }
        public List<Classes.Movie> GetNPopularMovies(int NumberOfMovies)
        {
            List<Classes.Movie> listMovies = new List<Classes.Movie>();

            var topn = (from rl in _context.Ratings
                        group rl by rl.movieId into g
                        select new
                        {
                            MovieId = g.Key,
                            Count = g.Count()
                        }).OrderByDescending(x => x.Count);
            var topUnique = (from x in topn select x).Distinct();
            foreach (var Movie in topUnique.Take(NumberOfMovies))
            {
                listMovies.Add(GetMovie(Movie.MovieId));
            }
            return listMovies;
        }
        public List<Classes.Movie> GetNBestMovies(int NumberOfMovies)
        {
            List<Classes.Movie> listMovies = new List<Classes.Movie>();

            var topn = (from rl in _context.Ratings
                        group rl by rl.movieId into g
                        select new
                        {
                            MovieId = g.Key,
                            AverageRating = g.Average(x => Convert.ToDecimal(x.rating, CultureInfo.InvariantCulture)),
                            NumberOfRating = g.Count()
                        }).OrderByDescending(x => x.NumberOfRating).Take(100);

            var tmp = topn.Where(a => a.NumberOfRating >= 150).OrderByDescending(x => x.AverageRating).Take(NumberOfMovies);
            foreach(var movie in tmp)

            {
                listMovies.Add(GetMovie(movie.MovieId));
            }

            return listMovies;
		}
        public List<Classes.Movie> FindAllMoviesByTitle(string wordInTitle)
        {
            var movieArray = _context.Movies.Where(p => p.title.Contains(wordInTitle));

            List<Classes.Movie> listMovies = new List<Classes.Movie>();
            foreach(EFModel.Movie movie in movieArray)
            {
                listMovies.Add(GetMovie(movie.movieId));
            }
            return listMovies;
        }
        public List<Classes.Movie> FindNMoviesByTitle(string wordInTitle, int n)
        {
            var movieArray = _context.Movies.Where(p => p.title.Contains(wordInTitle)).Take(n);

            List<Classes.Movie> listMovies = new List<Classes.Movie>();
            foreach (EFModel.Movie movie in movieArray)
            {
                listMovies.Add(GetMovie(movie.movieId));
            }
            return listMovies;
        }
        public List<Classes.Movie> GetMoviesByID(int[] movieIds)
        {
            List<Classes.Movie> lMovies = new List<Classes.Movie>();
            foreach (int id in movieIds)
            {
                lMovies.Add(GetMovie(id));
            }
            return lMovies;
        }
        public List<Classes.Movie> FindNMoviesByGenre(string genre, int iloscFilmow)
        {
            var movieArray = _context.Movies.Where(p => p.genres.Contains(genre)).Take(iloscFilmow);
            List<Classes.Movie> outputListMovies = new List<Classes.Movie>();
            foreach (EFModel.Movie movie in movieArray)
            {
                outputListMovies.Add(GetMovie(movie.movieId));
            }
            return outputListMovies;
        }

        public List<Classes.Movie> GetNRandomMoviesByGenre(int n, string genre)
        {
            List<Classes.Movie> listMovies = new List<Classes.Movie>();
            Random rnd = new Random();
            var topn = (from rl in _context.Ratings
                        group rl by rl.movieId into rlg
                        join ml in _context.Movies on rlg.FirstOrDefault().movieId equals ml.movieId
                        select new
                        {
                            MovieId = ml.movieId,
                            Genres = ml.genres,
                            Count = rlg.Count(),
                            Average = rlg.Average(x => Convert.ToDecimal(x.rating, CultureInfo.InvariantCulture)),
                        })
                        .Where(x => x.Genres.Contains(genre))
                        .OrderBy(x => Guid.NewGuid());
            var topUnique = (from x in topn select x).Distinct();
            foreach (var Movie in topUnique.Take(n))
            {
                listMovies.Add(GetMovie(Movie.MovieId));
            }
            return listMovies;
        }
        public List<Classes.Movie> GetNRandomMovies(int n)
        {
            List<Classes.Movie> listMovies = new List<Classes.Movie>();
            Random rnd = new Random();
            var topn = (from rl in _context.Ratings
                        group rl by rl.movieId into rlg
                        join ml in _context.Movies on rlg.FirstOrDefault().movieId equals ml.movieId
                        select new
                        {
                            MovieId = ml.movieId,
                            Genres = ml.genres,
                            Count = rlg.Count(),
                            Average = rlg.Average(x => Convert.ToDecimal(x.rating, CultureInfo.InvariantCulture)),
                        })
                       .OrderBy(x => Guid.NewGuid());
            var topUnique = (from x in topn select x).Distinct();
            foreach (var Movie in topUnique.Take(n))
            {
                listMovies.Add(GetMovie(Movie.MovieId));
            }
            return listMovies;
        }
    }
}
