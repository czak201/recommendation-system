﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Interfaces;
using WebApp.EFModel;
using WebApp.Classes;

namespace WebApp.Repository
{
    public class RatingRepository : iRatingRepository
    {
        private readonly EFContext _context;
        public RatingRepository(EFContext context)
        {
            _context = context;
        }

        public void Add(object obj)
        {
            
            _context.Add(obj);
            _context.SaveChanges();
        }

        public void AddList(RatingList model)
        {
            foreach (var m in model.Ratings)
            {
                _context.Add(m);
            }
            _context.SaveChanges();
        }

        public Rating Get(int id)
        {
            return _context.Ratings.FirstOrDefault(p => Convert.ToInt32(p.movieId) == id);
        }
    }
}
