﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.EFModel;
using WebApp.Interfaces;

namespace WebApp.Repository
{
    public class LinkRepository : iLinkRepository
    {
        private readonly EFContext _context;
        public LinkRepository(EFContext context)
        {
            _context = context;
        }

        public void Add(object obj)
        {
            _context.Add(obj);
            _context.SaveChanges();
        }

        public Link Get(int id)
        {
            return _context.Links.FirstOrDefault(p => Convert.ToInt32(p.movieId) == id);
        }
    }
}
