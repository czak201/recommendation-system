﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.EFModel;
using WebApp.Interfaces;

namespace WebApp.Repository
{
    public class UserRepository : iUserRepository
    {
        private readonly EFContext _context;

        // Aby ustawić wartość id dla użytkowników trza użyć querry w sqlexpress:
        //        DBCC CHECKIDENT('dbo.Users', RESEED, 672)
        //        GO

        public UserRepository(EFContext context)
        {
            _context = context;
        }

        public void Add(object obj)
        {
            _context.Add(obj);
            _context.SaveChanges();
        }
        public User GetUserById(int id)
        {
            return _context.Users.FirstOrDefault(p => p.userId == id);
        }
        public User GetUserByIdentity(string identity)
        {
            return _context.Users.FirstOrDefault(p => p.userIdentity == identity);
        }
    }
}
