﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Interfaces;
using WebApp.EFModel;

namespace WebApp.Repository
{
    public class TagRepository : iTagRepository
    {
        private readonly EFContext _context;
        public TagRepository(EFContext context)
        {
            _context = context;
        }

        public void Add(object obj)
        {
            _context.Add(obj);
            _context.SaveChanges();
        }

        public Tag Get(int id)
        {
            return _context.Tags.FirstOrDefault(p => Convert.ToInt32(p.movieId) == id);
        }
    }
}
