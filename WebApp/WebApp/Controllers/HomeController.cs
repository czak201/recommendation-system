﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Models;
using WebApp.Models.AccountViewModels;
using WebApp.Services;
using WebApp.EFModel;
using WebApp.Repository;
using WebApp.Classes;
using WebApp.Interfaces;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly UrlEncoder _urlEncoder;
        private readonly iRestClient _restClient;
        private readonly iMovieRepository _movieRepository;
        private readonly iUserRepository _userRepository;
        private readonly IConfiguration Configuration;


        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        private const string RecoveryCodesKey = nameof(RecoveryCodesKey);

        public HomeController(
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
          IEmailSender emailSender,
          ILogger<ManageController> logger,
          UrlEncoder urlEncoder,
          iRestClient restClient,
          iUserRepository userRepository,
          iMovieRepository movieRepository,
          IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _urlEncoder = urlEncoder;
            _restClient = restClient;
            _userRepository = userRepository;
            _movieRepository = movieRepository;
            Configuration = configuration;
        }

        public async Task<IActionResult> Index()
        {
            int movieCount = Configuration.GetValue<int>("MovieCount");
            var user = await _userManager.GetUserAsync(User);
            List<Article> articles = new List<Article>
            {
                new Article("Super Mega Umcyk Film \"Przestańcie hardkodować wartości\" niedługo w kinach!",
                "\'Jeden z najlepszych filmów dekady\' - Dosłownie Nikt",
                "\'Jeden z najlepszych filmów dekady\' - Dosłownie Nikt",
                "/images/movie-tv.jpg",
                "/images/movie-tv.jpg"
                ),
                new Article("Nowość w kinie meduza \"Projekt Studencki\" niedługo w kinach!",
                "Darmowe stockowe zdjęcia z pixabay!",
                "Darmowe stockowe zdjęcia z pixabay!",
                "/images/old-tv.jpg",
                "/images/old-tv.jpg"
                )
            };
            ViewData["ArticleList"] = articles;
            ViewData["PopularMovieList"] = _movieRepository.GetNPopularMovies(movieCount);
            ViewData["BestMovieList"] = _movieRepository.GetNBestMovies(movieCount);
            if(user!=null)
            {
                var userid = _userRepository.GetUserByIdentity(user.Id);
                ViewData["PredictedMovieList"] = _movieRepository.GetMoviesByID(_restClient.GetTopN(userid.userId, movieCount));
                ViewData["UserLogin"] = user.Email;
            }
            else
            {
                ViewData["PredictedMovieList"] = _movieRepository.GetMoviesByID(_restClient.GetNPredictEmptyUser(movieCount));
                ViewData["UserLogin"] = "niezalogowany";

            }
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
