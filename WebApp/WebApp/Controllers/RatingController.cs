﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.EFModel;
using WebApp.Repository;
using WebApp.Classes;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Identity;
using WebApp.Models;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Logging;
using System.Text.Encodings.Web;
using WebApp.Interfaces;
using Microsoft.Extensions.Configuration;

namespace WebApp.Controllers
{
    public class RatingController : Controller
    {
        //// get: rating
        //public actionresult index()
        //{
        //    return view();
        //}

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly iUserRepository _userRepository;
        private readonly iMovieRepository _movieRepository;
        private readonly iRestClient _restClient;
        private readonly iRatingRepository _ratingRepository;
        private readonly IConfiguration Configuration;



        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        private const string RecoveryCodesKey = nameof(RecoveryCodesKey);

        public RatingController(
          UserManager<ApplicationUser> userManager,
          iUserRepository userRepository,
          iMovieRepository movieRepository,
          iRestClient restClient,
          iRatingRepository ratingRepository,
          IConfiguration configuration)
        {
            _userManager = userManager;
            _userRepository = userRepository;
            _movieRepository = movieRepository;
            _restClient = restClient;
            _ratingRepository = ratingRepository;
            Configuration = configuration;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index(string returnUrl = null)
        {

            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> RateMovies(int nMovies, string genreMovies, string searchType, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            var userIdentity = await _userManager.GetUserAsync(User);
            var userId = _userRepository.GetUserByIdentity(userIdentity.Id);

            int[] topNMovieIdsArray;
            topNMovieIdsArray = _restClient.GetTopN(userId.userId, nMovies);
            ViewData["UserId"] = userId.userId;
            List<Classes.Movie> lMovies = new List<Classes.Movie>();
            if (genreMovies == null)
            {
                lMovies = _movieRepository.GetNRandomMovies(nMovies);
            }else
            {
                if (searchType == "Random")
                {
                    lMovies = _movieRepository.GetNRandomMoviesByGenre(nMovies, genreMovies);
                }else
                    if(searchType == "Popular")
                {
                    lMovies = _movieRepository.GetNPopularMoviesByGenre(nMovies, genreMovies);
                }
            }
            ViewData["MovieList"] = lMovies;
            var viewRatingModel = new RatingList();
            viewRatingModel.Ratings = new List<Rating>();
            for(int i = 0; i < nMovies; i++)
            {
                viewRatingModel.Ratings.Add(new Rating());
            }
            viewRatingModel.ratingList = new List<SelectRatingList>
            {
                new SelectRatingList{ Value = 0.2f, Display = "1.0"  },
                new SelectRatingList{ Value = 0.3f, Display = "1.5"  },
                new SelectRatingList{ Value = 0.4f, Display = "2.0"  },
                new SelectRatingList{ Value = 0.5f, Display = "2.5"  },
                new SelectRatingList{ Value = 0.6f, Display = "3.0"  },
                new SelectRatingList{ Value = 0.7f, Display = "3.5"  },
                new SelectRatingList{ Value = 0.8f, Display = "4.0"  },
                new SelectRatingList{ Value = 0.9f, Display = "4.5"  },
                new SelectRatingList{ Value = 1.0f, Display = "5.0"  }
            };
            return View(viewRatingModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RateMovies(RatingList model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            int timestampIndex = 0;
            foreach (Rating rating in model.Ratings)
            {
                rating.timestamp = int.Parse(DateTime.Today.ToString("MMddhhmmss"))+timestampIndex;
                timestampIndex++;
                _ratingRepository.Add(rating);
            }
            var userIdentity = await _userManager.GetUserAsync(User);
            var user = _userRepository.GetUserByIdentity(userIdentity.Id);
            if (userIdentity != null)
            {
                _ratingRepository.AddList(model);
                string myJson = _restClient.ReturnJsonForPython(user.userId, model);
                _restClient.UpdateUser(myJson);
            }

            // If we got this far, something failed, redisplay form
            return RedirectToAction(nameof(Index));
        }


        // GET: Rating/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Rating/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Rating/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Rating/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Rating/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Rating/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Rating/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}