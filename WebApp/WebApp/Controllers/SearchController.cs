﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using WebApp.Repository;
using WebApp.EFModel;
using WebApp.Interfaces;
using Microsoft.Extensions.Configuration;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly iMovieRepository _movieRepository;
        private readonly IConfiguration Configuration;

        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        private const string RecoveryCodesKey = nameof(RecoveryCodesKey);

        public SearchController(
          UserManager<ApplicationUser> userManager,
          iMovieRepository movieRepository,
          IConfiguration configuration)
        {
            _userManager = userManager;
            _movieRepository = movieRepository;
            Configuration = configuration;
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Search(string searchQuery, string returnUrl = null)
        {
            int movieCount = Configuration.GetValue<int>("MovieCount");
            List<Classes.Movie> listOutputMovies = new List<Classes.Movie>();
            listOutputMovies.AddRange(_movieRepository.FindNMoviesByTitle(searchQuery, movieCount));
            listOutputMovies.AddRange(_movieRepository.FindNMoviesByGenre(searchQuery, movieCount));

            ViewData["UserQuery"] = searchQuery;
            ViewData["SearchOutputMovieList"] = listOutputMovies;
            return View();
        }
    }
}
