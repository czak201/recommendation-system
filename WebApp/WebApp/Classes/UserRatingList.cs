﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Classes
{
    public class UserRatingList
    {
        private int _userId;
        private int _movieCount;
        private List<MiniRating> _listaOcenDlaFilmow = new List<MiniRating>();

        public UserRatingList(int userid)
        {
            user = userid;
        }

        public int user { get => _userId; set => _userId = value; }
        public List<MiniRating> oceny { get => _listaOcenDlaFilmow; set => _listaOcenDlaFilmow = value; }
        public int liczbaFilmow { get => _movieCount; set => _movieCount = value; }

        public void AddRatingForMovie(int idFilmu, float ocenaFilmu)
        {
            MiniRating tmp = new MiniRating(idFilmu, ocenaFilmu);
            oceny.Add(tmp);
            liczbaFilmow = oceny.Count();
        }

        public class MiniRating
        {
            private int _movieid;
            private float _movierating;

            public MiniRating(int movieid, float movierating)
            {
                movieId = movieid;
                rating = movierating;
            }

            public int movieId { get => _movieid; set => _movieid = value; }
            public float rating { get => _movierating; set => _movierating = value; }


        }
    }
}
