﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Classes
{
    public class Article
    {
        public string Title { get; set; }
        public string Snippet { get; set; }
        public string Body { get; set; }
        public string ImageLink { get; set; }
        public string ArticleLink { get; set; }
        public Article(string title, string snippet, string body, string imagelink, string articlelink)
        {
            Title = title;
            Snippet = snippet;
            Body = body;
            ImageLink = imagelink;
            ArticleLink = articlelink;
        }
        public Article()
        {
            Title = "Not Available";
            Snippet = "Snippet for this article is not available.";
            Body = "This article is not available.";
            ImageLink = "";
            ArticleLink = "";
        }
    }
}
