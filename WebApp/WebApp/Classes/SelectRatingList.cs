﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Classes
{
    public class SelectRatingList
    {
        public float Value { get; set; }
        public string Display { get; set; }
    }
}
