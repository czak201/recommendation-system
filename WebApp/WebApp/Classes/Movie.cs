﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Classes
{
    public class Movie
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public int NoOfRatings { get; set; }
        public float MeanOfRatings { get; set; }
        public string Genres { get; set; }
        public string Link { get; set; }
        public string IMDBLink { get; set; }
        public Movie()
        {
            ID = 0;
            Title = "Not Avaliable";
            Year = DateTime.Today.Year;
            NoOfRatings = 999;
            MeanOfRatings = 5.0f;
            Genres = new string("Adventure");
            Link = "/404";
        }

        public Movie(int id,string title,string genres, float meanRating, int numOfRatings, int imdb)
        {
            ID = (id > 0) ? id:0 ;
            string[] splitTitle = title.Split('(');
            //Getting production year from the title ex. Forest Gump (1994)
            Year = Convert.ToInt32(splitTitle[splitTitle.Length - 1].Remove(splitTitle[splitTitle.Length - 1].Length-1));
            //Getting just the title from the splitted input title 
            for (int i = 0; i < splitTitle.Length - 1; i++)
                Title += splitTitle[i];
            NoOfRatings =(int) numOfRatings;
            MeanOfRatings = (float) meanRating;
            //Tymczasowe rozwiazanie do poprawy
            Genres = genres;
            string zeros = "tt";
            for(int i = imdb.ToString().Length; i < 7; i++)
            {
                zeros += "0";
            }
            Link = "http://img.omdbapi.com/?apikey=9b311ca5&i=" + zeros + imdb.ToString();
            IMDBLink = "https://www.imdb.com/title/" + zeros + imdb.ToString();
        }
    }
}
