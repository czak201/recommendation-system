﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebApp.EFModel;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Microsoft.Extensions.Configuration;
using WebApp.Interfaces;

namespace WebApp.Classes
{
    public class RestClient : iRestClient
    {
        public IConfiguration Configuration { get; }
        public RestClient(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public string ReturnJsonForPython(int id, RatingList ratings)
        {
            var k = new UserRatingList(id);
            foreach (Rating rating in ratings.Ratings)
            {
                k.AddRatingForMovie(rating.movieId, rating.rating);
            }
            return JsonConvert.SerializeObject(k);
        }

        public void AddUser(string jsonMsg)
        {
            string url = Configuration.GetConnectionString("ApiConnection")+"newUser";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonMsg);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResp = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var stream = new StreamReader(httpResp.GetResponseStream()))
            {
                var result = stream.ReadToEnd();
            }
        }

        public void UpdateUser(string jsonMsg)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Configuration.GetConnectionString("ApiConnection") + "updateUser");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonMsg);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResp = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var stream = new StreamReader(httpResp.GetResponseStream()))
            {
                var result = stream.ReadToEnd();
            }
        }

        public int[] GetAllPred(int id)
        {
            try
            {
                JArray releases = null;
                string url = Configuration.GetConnectionString("ApiConnection") + "userAll/" + id;
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                    var response = httpClient.GetStringAsync(new Uri(url)).Result;
                    releases = JArray.Parse(response);
                }
                int[] tempOutput = releases.ToObject<int[]>();
                return tempOutput.Take(10).ToArray();
            } catch (Exception e)
            {
                return new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            }
        }

        public int[] GetTopN(int id, int n)
        {
            try
            {
                JArray releases = null;
                string url = Configuration.GetConnectionString("ApiConnection") + "userN/" + id + "/" + n;

                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                    var response = httpClient.GetStringAsync(new Uri(url)).Result;
                    releases = JArray.Parse(response);
                }
                int[] tempOutput = releases.ToObject<int[]>();
                return tempOutput.Take(n).ToArray();

            }
            catch(Exception e)
            {
                return new int[] { 1, 1, 1 };
            }
        }

        public void Remodel()
        {
            
            string url = Configuration.GetConnectionString("ApiConnection") + "remodel";
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                var response = httpClient.GetAsync(new Uri(url));
            }
        }

        public int[] GetNPredictEmptyUser(int n)
        {
            try
            {
                JArray releases = null;
                string url = Configuration.GetConnectionString("ApiConnection") + "emptyUser";
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                    var response = httpClient.GetStringAsync(new Uri(url)).Result;
                    releases = JArray.Parse(response);
                }
                int[] tempOutput = releases.ToObject<int[]>();
                return tempOutput.Take(n).ToArray();
            }
            catch(Exception e)
            {
                return new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            }
        }
    }
}
