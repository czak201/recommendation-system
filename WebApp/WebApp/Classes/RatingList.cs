﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.EFModel;

namespace WebApp.Classes
{
    public class RatingList
    {
        public RatingList()
        {
            Ratings = new List<Rating>();
        }

        [Display(Name = "User ID")]
        public List<EFModel.Rating> Ratings { get; set; }
        public List<Classes.SelectRatingList> ratingList { get; set; }
    }
}
