### 0.0 Wstępna konfiguracja.
### 0.1 Załaduj pliki projektu /python do Pycharm IDE.
### 0.2 Zainstaluj dodatkowe oprogramowanie, tak aby w *Project Interpreter*'ze były następujące biblioteki: Flask, Pandas, Numpy, Scipy, Json
### 0.3 W pliku REST.py zmień konfigurację IP na zgodną z Twoim systemem (ip, port).
### 0.4 W Pycharm IDE uruchom plik REST.py

### 1.0 Zainstaluj MSSql Server i zaimportuj do niego backup bazy danych RecSys z pliku dostępnego w folderze /BackUp Bazy Danych.
### 1.1 Załaduj pliki projektu /WebApp do środowiska Visual Studio. 
### 1.2 Skonfiguruj lokalny web serwer (np. IIS Express). 
### 1.3 W pliku konfiguracyjnym projektu ``appsettings.json`` wewnątrz działu ``ConnectionStrings`` zamień ``DefaultConnection`` zgodnie z twoim wcześniej ustawionym Serwerem MSSQL i zamień ``ApiConnection`` na adress twojego serwera REST skonfigurowanego w poprzednim rozdziale. (Opcjonalne) Możliwa jest zmiana ilości wyświetlanych filmów na stronie zmieniając wartość pola ``MovieCount`` na dowolną liczbę większą od zera.