
# 1. /Views

Folder /Views zawiera pliki .cshtml przedstawiające konstrukcję poszczególnych elementów portalu internetowego opartego o wzorzec MVC (Model-View-Controler) - Model-Widok-Kontroler.

## 1.1. /Shared 

Folder /Shared zawiera pliki .cshtml które są elementami powtarzającymi się na reszcie poszczególnych stron.

### 1.1.1. _Layout.cshtml

Layout.cshtml zawiera header strony, meta tagi, linki do stylesheetów, pasek nawigacji strony, kod wywołujący ciało strony oraz footer i kod wywołujący sekcję ze skryptami.
Pasek nawigacji poza odnośnikami do stron zawiera również pole wyszukiwarki filmów.

### 1.1.2. _LoginPartial.cshtml

_LoginPartial.cshtml zawiera konstrukcję części paska nawigacji zajmującej się odnośnikami do stron logowania i rejestracji dla niezalogowanego użytkownika oraz oceny filmów, wyświetlania profilu oraz wylogowywania dla zalogowanego użytkownika. 

## 1.2. /Home *(Strona Główna)*
### 1.2.1. Index.cshtml

Controller:``HomeController``
Index.cshtml reprezentuje układ strony głównej portalu, na której wyświetlane  są filmy proponowane użytkownikowi jak i najpopularniejsze, i najlepsze filmy z bazy danych. Wyświetlany jest również slider artykułów o filmach. Plik zawiera kod pobierający liste obiektów typu ``Classes.Movie`` oraz ``Classes.Article`` przesłanych z kontrolera ``HomeController.cs`` i wyświetla ich zawartość przy wykorzystaniu ``_MoviesPartial.cshtml`` oraz ``_ArticleSliderPartial.cshtml``. 

### 1.2.2. _MoviesPartial.cshtml

Plik zawiera kod przyjmujący model typu ``List<WebApp.Classes.Movie>`` i wyświetlający jego zawartość w postaci wierszy typu ``row`` z biblioteki Bootstrap zawierających po 6 elementów typu ``col-sm-2`` z podanego modelu na jeden wiersz. 

### 1.2.3. _ArticleSliderPartial.cshtml 

Plik zawiera kod przyjmujący model typu ``List<WebApp.Classes.Article>`` i wyświetlający jego zawartość w postaci slidera typu ``carousel`` z biblioteki Bootstrap.

## 1.3. /Rating *(Ocenianie Filmów)*

### 1.3.1. Index.cshtml 

Controller:``RatingController``
Plik opisuje układ strony wyświetlającej trzy pola wyboru puli filmów do oceny. Korzysta z pliku ``_GenresPartial.cshtml`` by wyświetlić listę dostępnych gatunków filmów.

### 1.3.2. _GenresPartial.cshtml

Plik wyświetla dostępne opcje gatunków filmu do  tagu select wykorzystywanego w pliku Index.cshtml strony Rating.

### 1.3.3. RateMovies.cshtml

Controller:``RatingController``
Plik zawiera kod wyświetlający liste filmów przesłaną z ``RatingController`` w postaci ``List<WebApp.Classes.Movie>``  oraz przyjmujący i przesyłający spowrotem model typu ``WebApp.Classes.RatingList`` wraz z ocenami użytkownika pozyskanymi w formatce do funkcji ``RateMovies`` metodą POST.

## 1.4. /Search

### 1.4.1. Search.cshtml

Controller:``SearchController``
Plik zawiera kod wyświetlający za pomocą ``_MoviesPartial.cshtml`` liste filmów przesłaną z ``RatingController`` w postaci ``List<WebApp.Classes.Movie>`` 

# 2. /css

Folder /css zawiera pliki z opisem stylów stron portalu.

# 3. /Controllers

Folder /Controllers zawiera pliki .cs przedstawiające kontrolery portalu internetowego opartego o wzorzec MVC (Model-View-Controler) - Model-Widok-Kontroler.

## 3.1. AccountController.cs

``async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)`` - Funkcja poza standardowym rejestrowaniem użytkownika przesyła również do modelu logicznego (python) informacje o nowym użytkowniku by uzupełnić tamtejszą macierz z ocenami o jeden nowy wiersz z ID nowego użytkownika. Funkcja rownież dodaje wzmianke do części bazy danych zajmującą się ocenami użytkowników pasującą do części bazy danych opartej o zbiór ``MovieLens``.

## 3.2. HomeController.cs

W tym pliku przygotowywane są filmy i artykuły do wyświetlenia na głównej stronie. Kontroller wykorzystuje dane z Bazy Danych ``RecSys`` otrzymane przy użyciu ``MovieRepository`` jak i dane przesłane z modelu logicznego (python)  otrzymane przy wykorzystaniu ``RestClient``. Listy filmów i artykułów są przesyłane do widoku przed jego wyświetleniem.

## 3.3. RatingController.cs 

Kontroler obsługuje ocenianie filmów przez użytkowników i wysyłanie ocen do bazy danych i modelu logicznego(python).
* Funkcja ``async Task<IActionResult> RateMovies(int nMovies, string genreMovies, string searchType, string returnUrl = null)`` przygotowywane są dane filmów do wyświetlenia użytkownikowi w celach przesłania ocen filmów. ``nMovies`` ,``searchType`` i ``genreMovies`` przechwytywane są z formatek ``Index.cs``. ``nMovies`` odpowiada za liczbę filmów do wyświetlenia, ``genreMovies`` odpowiada za gatunek wyświetlanych filmów a ``searchType`` wyznacza wybór algorytmu, którym będą wybierane filmy do oceny. Funkcja przesyła do formatki widoku ``RateMovies.cshtml`` model typu ``RatingList`` zawierający obiekt typu ``List<SelectRatingList>``, który ogranicza wartości podawanych ocen filmów.
* Funkcja ``async Task<IActionResult> RateMovies(RatingList model, string returnUrl = null)`` przechwytuje model typu ``RatingList`` z widoku ``RateMovies.cshtml``, który zawiera oceny wybranych filmów w raz z ID użytkownika, który je ocenił. Oceny użytkownika następnie są przesyłane kolejno do bazy danych i modelu logicznego(python) przy użyciu ``RatingRepository`` i ``RestClient``.

## 3.4. SearchController.cs

Kontroler obsługuje zapytanie użytkownika o filmy o danym tytule lub gatunku i przesyła do widoku ``Search.cs`` model typu ``List<Classes.Movie>`` zawierający liste filmów do wyświetlenia.

# 4. /Classes

## 4.1. Article.cs

# Pola klasy:
```
string Title { get; set; }
string Snippet { get; set; }
string Body { get; set; }
string ImageLink { get; set; }
string ArticleLink { get; set; }
```
``Title`` - Tytuł artykułu.
``Snippet`` - Skrót zawartości.
``Body`` - Pełna zawartość. 
``ImageLink`` - Odnośnik do obrazu rozróżniającego artykuł.
``ArticleLink`` - Odnośnik do pełnego artykułu.
# Funkcje klasy:
```
public Article(string title, string snippet, string body, string imagelink, string articlelink)
public Article()
```
``Article(*)`` - Konstruktory klasy.

## 4.2. Movie.cs
Klasa opisująca dokładnie informacje o filmach zawarte w bazie danych.
# Pola klasy:
```
int ID { get; set; }
string Title { get; set; }
int Year { get; set; }
int NoOfRatings { get; set; }
float MeanOfRatings { get; set; }
string Genres { get; set; }
string Link { get; set; }
string IMDBLink { get; set; }
```
``ID`` - Identyfikator filmu.
``Title`` - Pełny tytuł.
``year`` - Rok premiery. 
``NoOfRatings`` - Liczba ocen.
``MeanOfRatings`` - Średnia ocena.
``Genres`` - Gatunki.
``Link`` - Odnośnik do plakatu filmu.
``IMDBLink`` - Odnośnik do filmu na portalu IMDB.
# Funkcje klasy:
```
Movie()
Movie(int id,string title,string genres, float meanRating, int numOfRatings, int imdb)
```
``Movie()`` - Bezargumentowy konstruktor klasy
``Movie(int id,string title,string genres, float meanRating, int numOfRatings, int imdb)`` - Konstruktor wydzielający z podanych argumentów poszczególne dane dotyczące filmu.

## 4.3. RatingList.cs
Klasa służąca jako model formatki w ``RateMovies.cshtml``.
```
List<EFModel.Rating> Ratings { get; set; }
List<Classes.SelectRatingList> ratingList { get; set; }
```
``RatingList()`` - Bezargumentowy konstruktor klasy

## 4.4. SelectRatingList.cs
Klasa służąca do wyświetlenia jako część modelu w postaci opcji htmlowego tagu select.
```
float Value { get; set; }
string Display { get; set; }
```
``Value`` - Wartość opcji przesyłana przez formatkę.
``Display`` - Wyświetlana wartość opcji przesyłana przez formatkę.